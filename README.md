## Instucciones Compilacion
**docker-compose build**   
**docker-compose up**   

El resultado va a generar dentro del directorio **scripts** 2 directorios, **build** y **dist**.    
El binario para ejecutar es el disponible en **scripts/dist/testing-tool**


## Clase DBTools

Ejemplo de implementacion:   
```
DBPrincipalTesting  = DBTools('localhost', 'root', '1234', 'database_numero_uno')
DBPrincipalTesting.make_backup('ambiente_testing')

filename_backup_main=DBPrincipalTesting.get_filename_backup()

DBPrincipalDEV  = DBTools('localhost', 'root', '1234', 'database_numero_dos')
DBPrincipalDEV.apply_backup(filename_backup_main)
```