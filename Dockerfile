FROM centos:centos7 AS compiler
# FROM rockylinux:8.6

RUN yum install -y python3 gcc clang git zlib-devel make cmake && \
    yum clean all && \
    rm -rf /var/cache/yum

RUN pip3 install --no-cache-dir  setuptools && \
    pip3 install --no-cache-dir  wheel
RUN git clone -b v4.6 https://github.com/pyinstaller/pyinstaller.git && \
    cd /pyinstaller && \
    rm -rf .git && \
    python3 setup.py install && \
    cd / && rm -rf /pyinstaller
# RUN cd /pyinstaller/bootloader && python3 ./waf all --target-arch=64bit

FROM compiler AS builder

RUN useradd -u 1000 -m -s /bin/bash custom_user
USER custom_user
WORKDIR /app


ARG GIT_USERNAME=none
ARG GIT_PASSWORD=none
ARG REPO_URL=none
ARG REPO_TAG=master

ARG FOLDER_PROYECT=scripts
ARG BIN_NAME=testing-tool

ARG USE_MOUNT_PROYECT=false

RUN echo -e '#!/bin/bash \n echo username=${GIT_USERNAME} \n echo password=${GIT_PASSWORD}' > /tmp/credential-helper.sh && \
    chmod +x /tmp/credential-helper.sh && \
    git config --global  credential.helper '/bin/bash /tmp/credential-helper.sh'

# Esto deberia funcionar
RUN if [ "$USE_MOUNT_PROYECT" = "false" ]; then \
    git clone --depth 1 -b $REPO_TAG $REPO_URL . && \
    cd $FOLDER_PROYECT && \
    pyinstaller -n $BIN_NAME -y --clean --onefile  -s __main__.py; \
else \
exit 0; \
fi

CMD if [ "$USE_MOUNT_PROYECT" = "false" ]; then \
    cp -r /app/$FOLDER_PROYECT/dist/$BIN_NAME /build/; \
else \
exit 0; \
fi

# Esto es para pruebas
# COPY ./app .
# RUN pyinstaller -n $BIN_NAME -y --clean --onefile  -s __main__.py

# Esto es una herramienta para dejar el contenedor corriendo
# ENTRYPOINT ["tail", "-f", "/dev/null"]



# Ejecutar diferentes comandos según el valor de ENVIRONMENT
CMD if [ "$ENVIRONMENT" = "dev" ]; then \
        echo "Ejecutando en modo de desarrollo"; \
    else \
        echo "Ejecutando en modo de producción"; \
    fi