import os, sys
from src.Helper.MariaDBTools import MariaDBTools
from src.Helper.MariaDBUpdate import MariaDBUpdate

class Routines:
  def __init__(self, routine, args):
    self.routine = routine
    
    proyect_directory = os.getenv('PROYECT_DIRECTORY', 'empty')
    try:
      os.chdir(proyect_directory)
      print("Cambiando a directorio: "+proyect_directory)
    except:
      print("Error al cambiar al directorio: "+proyect_directory)
      sys.exit(1)
    
    self.origin_db_host = os.getenv('ORI_DB_HOST', 'empty')
    self.origin_db_user = os.getenv('ORI_DB_USER', 'empty')
    self.origin_db_pass = os.getenv('ORI_DB_PASS', 'empty')
    self.origin_db_name = os.getenv('ORI_DB_NAME', 'empty')

    self.target_db_host = os.getenv('TARG_DB_HOST', 'empty')
    self.target_db_user = os.getenv('TARG_DB_USER', 'empty')
    self.target_db_pass = os.getenv('TARG_DB_PASS', 'empty')
    self.target_db_name = os.getenv('TARG_DB_NAME', 'empty')
    
    self.sufix_log = '_log'
    if(self.origin_db_name == 'sigarhu' or self.target_db_name == 'sigarhu'):
      self.sufix_log = '_aud'
    
    
    do = f"{routine}"
    if hasattr(self, do):# and callable(func := getattr(self, do)):
      func()
    pass


  def makeBackupDB(self):
    try:
      DBPrincipalTesting  = MariaDBTools(self.origin_db_host, self.origin_db_user, self.origin_db_pass, self.origin_db_name)
      DBPrincipalTesting.make_backup('backup-automatico')
    except:
      print('Error al clonar la base de datos principal.')
      sys.exit(1)

    try:
      DBPrincipalTesting  = MariaDBTools(self.origin_db_host, self.origin_db_user, self.origin_db_pass, self.origin_db_name+self.sufix_log)
      DBPrincipalTesting.make_backup('backup-automatico')
    except:
      # print('Error al clonar la base de datos secundaria.')
      sys.exit(0)
      

  def restoreBackupDB(self):
    try:
      DBPrincipalTesting  = MariaDBTools(self.origin_db_host, self.origin_db_user, self.origin_db_pass, self.origin_db_name)
      DBPrincipalTesting.apply_backup(self.origin_db_name+'-backup-automatico.sql')
    except:
      print('Error al clonar la base de datos principal.')
      sys.exit(1)

    try:
      DBPrincipalTesting  = MariaDBTools(self.origin_db_host, self.origin_db_user, self.origin_db_pass, self.origin_db_name+self.sufix_log)
      DBPrincipalTesting.apply_backup(self.origin_db_name+self.sufix_log+'-backup-automatico.sql')
    except:
      # print('Error al clonar la base de datos secundaria.')
      sys.exit(0)


  def cloneDB(self):    
    try:
      DBPrincipalTesting  = MariaDBTools(self.origin_db_host, self.origin_db_user, self.origin_db_pass, self.origin_db_name)
      DBPrincipalTesting.make_backup('testing')

      DBPrincipalDEV  = MariaDBTools(self.target_db_host, self.target_db_user, self.target_db_pass, self.target_db_name)
      DBPrincipalDEV.apply_backup(DBPrincipalTesting.get_filename_backup())
    except:
      print('Error al clonar la base de datos principal.')
      sys.exit(1)

    try:
      DBPrincipalTesting  = MariaDBTools(self.origin_db_host, self.origin_db_user, self.origin_db_pass, self.origin_db_name+self.sufix_log)
      DBPrincipalTesting.make_backup('testing')

      DBPrincipalDEV  = MariaDBTools(self.target_db_host, self.target_db_user, self.target_db_pass, self.target_db_name+self.sufix_log)
      DBPrincipalDEV.apply_backup(DBPrincipalTesting.get_filename_backup())
    except:
      # print('Error al clonar la base de datos secundaria.')
      sys.exit(0)
  
  
  def updateDB(self):
    
    
    # update  = MariaDBUpdate(self.origin_db_host, self.origin_db_user, self.origin_db_pass, self.origin_db_name)

    # print(update.get_dir_tmp())
    # print(update.rm_dir_tmp())
    # print(update.filtrar_versiones())
    pass
