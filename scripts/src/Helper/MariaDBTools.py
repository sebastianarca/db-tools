import os, subprocess

class MariaDBTools:
  '''
  Herramientas para el manejo y administracion de MariaDB
  '''
  # Defino parametros de conexion
  def __init__(self, db_host, db_user, db_pass, db_name):
    self.db_host=db_host
    self.db_user=db_user
    self.db_pass=db_pass
    self.db_name=db_name
    self.filename_backup=''


  def get_filename_backup(self) -> str:
    '''
    Obtener nombre del archivo de backup
    '''
    return self.filename_backup


  def exec_query(self, query: str):
    '''
    Ejecutar una query determinada
    '''
    try:
      subprocess.check_output(['mysql -h '+self.db_host+' -u '+self.db_user+' -p'+self.db_pass+' -e "'+query+'"'], shell=True)
      return True
    except:
      return False
  
  
  def get_query_value(self, query: str) -> str:
    '''
    Obtener el valor de una query
    '''
    try:
      result = subprocess.check_output(['mysql -rsN -h '+self.db_host+' -u '+self.db_user+' -p'+self.db_pass+' -e "'+query+'"'], shell=True)
      result = result.decode('utf-8')
      return result
    except:
      raise Exception('Error al ejecutar la query: '+query)
  
  
  def check_db_exist(self) -> bool:
    '''
    Verificar que la DB elegida exista
    '''
    query='use '+self.db_name+';'
    if self.exec_query(query) == True:
      print('Base de datos '+self.db_name+' encontrada.')
      return True
    else:
      print('Base de datos '+self.db_name+' no existe.')
      return False
  
  
  def check_db_empty(self) -> bool:
    '''
    Verificar si la DB esta limpia o tiene tablas
    '''
    query = 'SELECT COUNT(DISTINCT table_name) FROM information_schema.columns WHERE table_schema = \''+self.db_name+'\''

    try:
      result = self.get_query_value(query)
      if result == '0':
        print('Base de datos '+self.db_name+' esta vacia.')
        return True
      else:
        print('Base de datos '+self.db_name+' tiene tablas.')
        return False
    except:
      raise Exception('Error al verificar si la base de datos '+self.db_name+' esta vacia.')
  
  
  def reset_db(self, check_exist:bool=True) -> bool:
    ''''
    Borrar y recrear la DB elegida
    '''
    if(check_exist == True ):
      if( self.check_db_exist() == False ):
        return False

    query = [
      'DROP DATABASE IF EXISTS '+self.db_name+' ;',
      'CREATE DATABASE IF NOT EXISTS '+self.db_name+' CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;'
    ]
    query=' '.join(query)

    print('Recreando base de datos '+self.db_name+'...')
    if self.exec_query(query) == True:
      print('Base de datos '+self.db_name+' creada.')
      return True
    else:
      print('Error al crear base de datos '+self.db_name+'.')
      return False
  
  
  def make_backup(self, sufix_filename: str) -> bool:
    '''
    Realizar backup completo de la DB elegida, y volcarlo en un archivo SQL con el nombre indicado
    '''

    self.filename_backup=self.db_name+'-'+sufix_filename+'.sql'
    if( self.check_db_exist() == False ):
      return False

    params = ['--force', '--quote-names', '--dump-date', '--opt', '--single-transaction', '--events', '--routines', '--triggers', '--result-file='+self.filename_backup]
    params=' '.join(params)

    print('Realizando backup de la base de datos '+self.db_name+'...')
    try:
      subprocess.check_output(['mysqldump -h '+self.db_host+' -u '+self.db_user+' -p'+self.db_pass+' '+self.db_name+' '+params], shell=True)
      print('Backup realizado.')
      return True
    except:
      print('Error al realizar backup.')
      return False
  
  
  def apply_backup(self, filename: str) -> bool:
    '''
    Aplicar backup de la DB elegida, desde un archivo SQL con el nombre indicado
    '''
    
    if( os.path.isfile(filename) == False ):
      print('Archivo de Backup no encontrado: ' + filename)
      return False
    
    if( self.reset_db(check_exist=False) == False ):
      return False
    
    print('Aplicando backup de la base de datos '+self.db_name+'...')
    try:
      subprocess.check_output(['mysql -h '+self.db_host+' -u '+self.db_user+' -p'+self.db_pass+' '+self.db_name+' < '+filename], shell=True)
      print('Backup aplicado.')
      print('Borrando archivo de Backup. ' + filename)
      subprocess.check_output(['rm -rf '+filename], shell=True)
      print('Archivo de Backup borrado: ' + filename)
      return True
    except:
      print('Error al aplicar backup.')
      return False