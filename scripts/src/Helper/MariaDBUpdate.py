import os, time
from src.Helper.MariaDBTools import MariaDBTools

DIR_SQL_VERSION='sql/versiones'

class MariaDBUpdate:
  # Defino parametros de conexion
  def __init__(self, db_host, db_user, db_pass, db_name):
    self.db_host=db_host
    self.db_user=db_user
    self.db_pass=db_pass
    self.db_name=db_name
    self.filename_backup=''
    self.dir_tmp_sql=''
    self.db_tools = MariaDBTools(db_host, db_user, db_pass, db_name)
  
  
  # Verificar que el directorio de versiones exista
  def check_dir(self):
    if( os.path.isdir(DIR_SQL_VERSION) == False ):
      print('Directorio "'+DIR_SQL_VERSION+'" no fue encontrado.')
      return False
    else:
      return True
  
  
  # Obtener la version de SQL aplicada
  def get_version(self):
    db_version='0'
    if(self.db_tools.check_db_empty() == True):
      return db_version

    query="select version from "+self.db_name+".db_version where version=(select max(version) from "+self.db_name+".db_version)"
    
    try:
      result = self.db_tools.get_query_value(query)
      # Los resultados pueden ser 2, 2.0
      result = result.split('.')
      result = result[0]
      print('Version de la DB: '+result)
      return result
    
    except:
      raise Exception('La DB no tiene tabla version.')
  
  
  # Devuelve lista numerica ordenada de menor a mayor, correspondiente a los archivos a ejecutar separados por espacio. Ej: para los archivo "v12.sql, v_11.sql" devuelve "11 12"
  def get_list_versiones(self):
    list_versiones = []
    for file in os.listdir(DIR_SQL_VERSION):
      if(file.endswith(".sql")):
        list_versiones.append(file.replace('v','').replace('.sql','').replace('_',''))
    list_versiones.sort()
    return list_versiones
  
  
  def filtrar_versiones(self):
    list_versiones = self.get_list_versiones()
    db_version = self.get_version()
    list_versiones = list(filter(lambda x: int(x) > int(db_version), list_versiones))
    return list_versiones
  
  
  # Crea y retorna un directorio temporal con el nombre de la base de datos y una marca de tiempo
  def get_dir_tmp(self):
    if self.dir_tmp_sql != '':
      return self.dir_tmp_sql

    self.dir_tmp_sql = '/tmp/'+self.db_name+'_'+str(int(time.time()))
    if not os.path.exists(self.dir_tmp_sql):
      os.makedirs(self.dir_tmp_sql)
    return self.dir_tmp_sql


  # Elimina el directorio temporal y todo su contenido
  def rm_dir_tmp(self):
    if self.dir_tmp_sql == '':
      return False
    for file in os.listdir(self.dir_tmp_sql):
      os.remove(self.dir_tmp_sql+'/'+file)
    os.rmdir(self.dir_tmp_sql)
    return True


  # Compilar archivos SQL
  # Si no hay archivos para compilar imprime mensaje y corta ejecucion, caso contrario
  # Crea un directorio temporal /tmp/, contruye rutas de archivo original y destino de la copia,
  # Vuelca el contenido del archivo original hacia el archivo de destino reemplazando variables db_app, db_log, user_mysql. 
  # Comprueba que el archivo tenga el string db_version, caso contrario corta ejecucion.
  # intenta aplicar la primer iteracion en mariadb.
  # Elimina archivos temporales.
  def compilar_sql(self):
    list_versiones = self.filtrar_versiones()
    if(len(list_versiones) == 0):
      print('La base de datos funciona y esta acutalizada.')
      return False
    
    for version in list_versiones:
      print('Version a actualizar: '+version)
      filename = DIR_SQL_VERSION+'/v'+version+'.sql'
      if not os.path.isfile(filename):
        filename = DIR_SQL_VERSION+'/v_'+version+'.sql'
      if not os.path.isfile(filename):
        print('Archivo "'+filename+'" no fue encontrado.')
        raise Exception('Archivo "'+filename+'" no fue encontrado.')

      filename_tmp = self.get_dir_tmp()+'/v'+version+'.sql'

      with open(filename, 'r') as file:
        filedata = file.read()
      filedata = filedata.replace('{{{db_app}}}', self.db_name)
      filedata = filedata.replace('{{{db_log}}}', self.db_name+'_log')
      filedata = filedata.replace('{{{user_mysql}}}', self.db_user)

      with open(filename_tmp, 'w') as file:
        file.write(filedata)
      if(filedata.find('db_version') == -1):
        raise Exception('Archivo "'+filename+'" no contiene la palabra "db_version".')

      try:
        self.db_tools.apply_sql(filename_tmp)
      except:
        # Muestra el contenido del archivo en consola
        with open(filename_tmp, 'r') as file:
          print(file.read())

        raise Exception('Error al aplicar archivo "'+filename+'".')
      os.remove(filename_tmp)
    return True