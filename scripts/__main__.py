from src.Routines import Routines

import argparse

options = [
  "cloneDB - Clona la base de datos principal y secundaria desde el ambiente de Producción.",
  "makeBackupDB - Crea un respaldo de la base de datos principal y secundaria.",
  "restoreBackupDB - Restaura la copia de la base de datos principal y secundaria.",
  # "updateDB - Actualiza la base de datos principal y secundaria desde el ambiente de Producción.",
]
parser = argparse.ArgumentParser(
  prog='testing-tools', 
  description='Herramientas para el mantentenimiento del ambiente de Testing',
  epilog='\n'.join(options),
  formatter_class=argparse.RawTextHelpFormatter
)
parser.add_argument('-e', '--exec', 
  help="Comando que se desea ejecutar",
  default=False)

if __name__ == '__main__':
  cmd = vars(parser.parse_args())

  if(cmd['exec'] == False):
    print('Debe especificar un comando a ejecutar, use -h para ver la ayuda.')
  else:
    Routines(cmd['exec'])


